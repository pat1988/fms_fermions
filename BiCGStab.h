#ifndef _BIICGSTAB_H
#define _BIICGSTAB_HG

const int nsitecolor = nsite*ndirac;
const int NALL = nsite*ndirac*ncolor;

/******************************************************************************/

// IInitialize array of neighbors of every lattice point
void neib_init(int lengX, int lengY, int lengZ, int lengT);
// Action of the Wilson-Dirac operator on some vector
void Dirac_on_vec(complex<long double> *out, complex<long double> *in,
	              long double kappa, long double diag);
// Scalar product of two complex vetors
complex<long double> dot_product(complex<long double> *in1,
	                             complex<long double> *in2);

/******************************************************************************/

// Bi-Conjugate gradient with stabilization from:
// B. Jegerlehner, Nucl. Phys. B (Proc. Suppl.) 63, 958 (1998); hep-lat/9612014
void bicgstab(complex<long double> *b, complex<long double> *x,
	          long double kappa, long double diag)
{
	complex<long double> r0[NALL], r1[NALL], y0[NALL], s0[NALL],
	                     t1[NALL], t2[NALL], mv[NALL], ss0[NALL];
	complex<long double> bs0, esm1, es0, es1, rhos0, rhos1, as1, chis0, s,
	                     a0, a1, bm1, b0, d0, d1, phi0, chi0;
	long double residue;
	int iter = 0;
	bool done = false;

	/* s can be used if multiple masses should be computed. Then, this should
	   be an array with length #masses. This implies that also bs0, esm1, es0,
	   es1, rhos0, rhos1, as1, chis0 should be such arrays and ss0 and x should
	   have an additional index accounting for these multiple masses. For
	   details, consult the paper!

	   Translation of variables to Jegerlehner:
	   s              : sigma
	   r0,    r1      : r_i           , r_i+1
	   d0,    d1      : delta_i       , delta_i+1
	   a0,    a1, as0 : alpha_i       , alpha_i+1     , alpha_i+1^sigma
	   bm1,   bs0     : beta_i-1      , beta_i^sigma
	   s0,    ss0     : s_i           , s_i^sigma
	   esm1,  es0     : zeta_i-1^sigma, zeta_i^sigma
	   rhos0, rhos1   : rho_i^sigma   , rho_i+1^sigma
	   b0             : beta_i
	   phi0           : phi_i
	   t1,    t2, mv  : w_i+1         , D*w_i+1       , D*s_i
	   chi0           : chi_i
	*/

	s = 0;

	for(int id = 0; id < NALL; id++)
	{
		r0[id] = b[id];
		s0[id] = b[id];
		y0[id] = 1.0L + 0.1L*real(b[id]);
		ss0[id] = b[id];
		x[id] = 0.0L;
	}
	d0 = dot_product(y0,r0);

	if(abs(d0) < 10e-12)
	{
		printf("[BiCGStab] Error: d0 too small at iteration # %d.\n",iter);
		done = true;
	}

	d1 = d0;
	a0 = 0.0L;
	bm1 = 1.0L;
	esm1 = 1.0L;
	es0 = 1.0L;
	rhos0 = 1.0L;

	while(iter < niter_bicstab && done == false)
	{
		Dirac_on_vec(mv,s0,kappa,diag);
		phi0 = dot_product(y0,mv)/d1;
		b0 = -1.0L/phi0;
		for(int id = 0; id < NALL; id++) t1[id] = r0[id] + b0*mv[id];

		Dirac_on_vec(t2,t1,kappa,diag);
		es1 = es0*esm1*bm1/(b0*a0*(esm1-es0) + esm1*bm1*(1.0L-s*b0));
		bs0 = b0*es1/es0;
		chi0 = dot_product(t2,t1)/dot_product(t2,t2);
		chis0 = chi0/(1.0L + chi0*s);
		rhos1 = rhos0/(1.0L + chi0*s);
		for(int id = 0; id < NALL; id++)
		{
			r1[id] = t1[id] - chi0*t2[id];
			x[id] = x[id] - bs0*ss0[id] + chis0*es1*rhos0*t1[id];
		}

		d1 = dot_product(y0,r1);
		a1 = -b0*d1/(d0*chi0);
		as1 = a1*es1*bs0/(es0*b0);
		for(int id = 0; id < NALL; id++)
		{
			s0[id] = r1[id] + a1*(s0[id] - chi0*mv[id]);
			ss0[id] = es1*rhos1*r1[id] + as1*
			 (ss0[id] - chis0*(es1*rhos0*t1[id] - es0*rhos0*r0[id])/bs0);
		}

		residue = sqrt(real(dot_product(r1,r1)));
		if(residue < bicstab_tol)
		{
			printf("[BiCGStab] Finished BiCGStab at iteration # %d.\n",iter);
			done = true;
		}

		for(int id = 0; id < NALL; id++) r0[id] = r1[id];
		a0 = a1;
		bm1 = b0;
		d0 = d1;
		esm1 = es0;
		es0 = es1;
		rhos0 = rhos1;
		iter++;
	}
}

/******************************************************************************/

// Action of the Wilson-Dirac operator on some vector
// Note: Gamma matrices from Gattringer and Lang Book
void Dirac_on_vec(complex<long double> *out, complex<long double> *in,
	              long double kappa, long double diag)
{
	// neighboring lattice points
	int isp1, isp2, isp3, isp4;
	int ism1, ism2, ism3, ism4;

	// coefficients for Wilson Dirac operator
	complex<long double> as1141, as1242, as1333, as1434,
						 ad1141, ad1242, ad1333, ad1434,
						 as2232, as2343, as2444, as2131,
						 ad2232, ad2343, ad2444, ad2131;

	// diagonal elements
    for(int id = 0; id < NALL; id++) out[id] = diag*in[id];

	// off-diagonal elements
	for(int is = 0; is < nsite; is++)
	{
		// neighbors to "is"
		isp1 = neib[is][0];
		isp2 = neib[is][1];
		isp3 = neib[is][2];
		isp4 = neib[is][3];
		ism1 = neib[is][4];
		ism2 = neib[is][5];
		ism3 = neib[is][6];
		ism4 = neib[is][7];

		for(int c1 = 0; c1 < ncolor; c1++)
		{
			int c1n = c1*nsitecolor;
			for(int c2 = 0; c2 < ncolor; c2++)
			{
				int c2n = c2*nsitecolor;
				// compute coefficients (index = is + mu*nsite + c*nsite*ndirac)
				as1141 = U[is][0][c1][c2]
			  	        *(in[isp1+c2n] + II*in[isp1+3*nsite+c2n]);
				ad1141 = conj(U[ism1][0][c2][c1])
  			  	        *(in[ism1+c2n] - II*in[ism1+3*nsite+c2n]);
				as1242 = U[is][1][c1][c2]
			  	        *(in[isp2+c2n] + in[isp2+3*nsite+c2n]);
				ad1242 = conj(U[ism2][1][c2][c1])
			  	        *(in[ism2+c2n] - in[ism2+3*nsite+c2n]);
				as1333 = U[is][2][c1][c2]
			  	        *(in[isp3+c2n] + II*in[isp3+2*nsite+c2n]);
				ad1333 = conj(U[ism3][2][c2][c1])
  			  	        *(in[ism3+c2n] - II*in[ism3+2*nsite+c2n]);
				as1434 = U[is][3][c1][c2]
			  	        *(in[isp4+c2n] - in[isp4+2*nsite+c2n]);
				ad1434 = conj(U[ism4][3][c2][c1])
			  	        *(in[ism4+c2n] + in[ism4+2*nsite+c2n]);
				as2131 = U[is][0][c1][c2]
			  	        *(in[isp1+nsite+c2n] + II*in[isp1+2*nsite+c2n]);
				ad2131 = conj(U[ism1][0][c2][c1])
  			  	        *(in[ism1+nsite+c2n] - II*in[ism1+2*nsite+c2n]);
				as2232 = U[is][1][c1][c2]
			  	        *(in[isp2+nsite+c2n] - in[isp2+2*nsite+c2n]);
				ad2232 = conj(U[ism2][1][c2][c1])
  			 	        *(in[ism2+nsite+c2n] + in[ism2+2*nsite+c2n]);
				as2343 = U[is][2][c1][c2]
			 	        *(in[isp3+nsite+c2n] - II*in[isp3+3*nsite+c2n]);
				ad2343 = conj(U[ism3][2][c2][c1])
 			 	        *(in[ism3+nsite+c2n] + II*in[ism3+3*nsite+c2n]);
				as2444 = U[is][3][c1][c2]
			 	        *(in[isp4+nsite+c2n] - in[isp4+3*nsite+c2n]);
				ad2444 = conj(U[ism4][3][c2][c1])
 			 	        *(in[ism4+nsite+c2n] + in[ism4+3*nsite+c2n]);

				// add coefficients to the vector
				out[is        +c1n] -= kappa*
				                    (    as1141 + as1242 +   as1333 + as1434
			                         +   ad1141 + ad1242 +   ad1333 + ad1434 );
				out[is + nsite+c1n] -= kappa*
				                    (    as2131 + as2232 +   as2343 + as2444
			                         +   ad2131 + ad2232 +   ad2343 + ad2444 );
				out[is+2*nsite+c1n] -= kappa*
				                    (- II*as2131 - as2232 - II*as1333 - as1434
			                         + II*ad2131 + ad2232 + II*ad1333 + ad1434 );
			    out[is+3*nsite+c1n] -= kappa*
				                    (- II*as1141 + as1242 + II*as2343 - as2444
			                         + II*ad1141 - ad1242 - II*ad2343 + ad2444 );
		}}
	}
}

/******************************************************************************/

// Scalar product of two complex vectors
complex<long double> dot_product(complex<long double> *in1,
	                             complex<long double> *in2)
{
    complex<long double> out = 0.0;
    for(int i = 0; i < NALL; i++) out += conj(in1[i])*in2[i];

    return out;
}

/******************************************************************************/

// IInitialization of the neighbors of every lattice point
void neib_init(int lengX, int lengY, int lengZ, int lengT)
{
	int x, xp, xm, y, yp, ym, z, zp, zm, t, tp, tm;
	int is, isp1, ism1, isp2, ism2, isp3, ism3, isp4, ism4;

	for(x = 0; x < lengX; x++)
	{
  		xp = x + 1;
  		xm = x - 1;
  		if(x == lengX - 1) xp = 0;
  		if(x == 0)         xm = lengX - 1;

  		for(y = 0; y < lengY; y++)
  		{
  			yp = y + 1;
  			ym = y - 1;
  			if(y == lengY - 1) yp = 0;
  			if(y == 0)         ym = lengY - 1;

  			for(z = 0; z < lengZ; z++)
  			{
  				zp = z + 1;
  				zm = z - 1;
  				if(z == lengZ - 1) zp = 0;
  				if(z == 0)         zm = lengZ - 1;

  				for(t = 0; t < lengT; t++)
  				{
  					tp = t + 1;
  					tm = t - 1;
  					if(t == lengT - 1) tp = 0;
  					if(t == 0)         tm = lengT - 1;

  					is   = x  + y*lengX  + z*lengX*lengY  + t*lengX*lengY*lengZ;
  					isp1 = xp + y*lengX  + z*lengX*lengY  + t*lengX*lengY*lengZ;
  					ism1 = xm + y*lengX  + z*lengX*lengY  + t*lengX*lengY*lengZ;
  					isp2 = x  + yp*lengX + z*lengX*lengY  + t*lengX*lengY*lengZ;
  					ism2 = x  + ym*lengX + z*lengX*lengY  + t*lengX*lengY*lengZ;
  					isp3 = x  + y*lengX  + zp*lengX*lengY + t*lengX*lengY*lengZ;
  					ism3 = x  + y*lengX  + zm*lengX*lengY + t*lengX*lengY*lengZ;
  					isp4 = x  + y*lengX  + z*lengX*lengY  + tp*lengX*lengY*lengZ;
  					ism4 = x  + y*lengX  + z*lengX*lengY  + tm*lengX*lengY*lengZ;

  					neib[is][0] = isp1;
  					neib[is][1] = isp2;
  					neib[is][2] = isp3;
  					neib[is][3] = isp4;
  					neib[is][4] = ism1;
  					neib[is][5] = ism2;
  					neib[is][6] = ism3;
  					neib[is][7] = ism4;
  				}
  			}
  		}
	}
}

/******************************************************************************/

#endif
