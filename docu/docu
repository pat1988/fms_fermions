\documentclass[10pt]{article}
\usepackage{authblk}
\usepackage{hyperref} 
\usepackage{cite}
\usepackage{epsfig}
\usepackage{bm}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage{amsmath}
\usepackage{dsfont}
\usepackage{color}
\usepackage{pdfpages}
\usepackage{slashed}
\usepackage{bbm}
\usepackage[top=3cm, bottom=3cm, left=3cm, right=2cm]{geometry}

\begin{document}

\title{\LARGE{\bf Quenched vectorial fermions on the lattice}}

\vspace{25mm}


\author{\vskip13mm 
Pascal T\"orek\thanks{\href{mailto:pascal.toerek@uni-graz.at}{pascal.toerek@uni-graz.at}}}

\vspace{10mm}

\affil{{\small Institute of Physics, NAWI Graz, University of Graz, Universit\"atsplatz 5, 8010 Graz, Austria \vspace{3mm}}

\vspace*{0.5cm}
{\bf Code and Documentation:} $\texttt{gitlab.com/pat1988/fms\_fermions}$
}


\begin{titlepage}
\maketitle
\thispagestyle{empty}

\end{titlepage}

\section{Vectorial fermions in a Higgsed SU(2) gauge theory}

We consider an SU$(2)$ gauge theory with a Higgs field in the fundamental representation. We add additionally two vectorial fermion flavors, i.e., $\psi$ which are gauged and $\chi$ which are ungauged. 

The action in Minkowski space is given by 

\begin{align}
\begin{split}
S = \int\mathrm{d}^4 x\,\Big[
&-\frac{1}{4}F_{\mu\nu}F^{\mu\nu} + 
\big(D_\mu\phi\big)^\dagger\big(D^\mu\phi\big) 
+ \overline{\psi}\,\big(\mathrm{i}\slashed{D}-m_\psi\big)\,\psi+
\overline{\chi}_{\bar{k}}\,\big(\mathrm{i}\slashed{\partial}-m_{\chi_{\bar{k}}}\big)\,\chi_{\bar{k}}  \\
&- h_1\big(\overline{\psi}\tilde{\phi}\chi_1 
+ \overline{\chi}_1\tilde{\phi}^\dagger\psi\big)
- h_2\big(\overline{\psi}\phi\chi_2 
+ \overline{\chi}_2\phi^\dagger\psi\big)
- V(\phi^\dagger\phi)
\Big]\;,
\end{split}
\end{align}
where $\tilde{\phi} = \epsilon\phi^\star$, and $D_\mu = \partial_\mu+\mathrm{i}gA_\mu^a T^a$, with $T^a$ the SU$(2)$ generators.

Note, that barred indices are not local SU$(2)$ indices, they just label the different singlets $\chi_1$ and $\chi_2$. 

If $h_1=h_2$ and $m_\psi = m_{\chi_1} = m_{\chi_2} = 0$ the system obeys a discrete chiral symmetry which acts simultaneously on all fermion fields as
\begin{align}
\psi\to\mathrm{e}^{\mathrm{i}\frac{\pi}{2}\gamma_5}\psi\qquad,\qquad \chi_{\bar{i}}\to\mathrm{e}^{\mathrm{i}\frac{\pi}{2}\gamma_5}\chi_{\bar{i}}\;.
\end{align}

Applying the Higgs mechanism with 
\begin{align}
\phi &= \frac{v}{\sqrt{2}}\begin{pmatrix}0 \\ 1\end{pmatrix} + \varphi\qquad,\qquad
\tilde{\phi} = \frac{v}{\sqrt{2}}\begin{pmatrix}1 \\ 0\end{pmatrix} + \tilde{\varphi}\;,
\end{align}
gives rise to the following mass matrix for the fermion fields with the convention 
$\big(\psi_1\;\psi_2\;\chi_1\;\chi_2\big)^\mathrm{T}$:
\begin{align}
    M &= \begin{pmatrix}
         m_\psi                &0                     &\frac{v}{\sqrt{2}}h_1 &0        \\
         0                     &m_\psi                &0                     &\frac{v}{\sqrt{2}}h_2 \\
         \frac{v}{\sqrt{2}}h_1 &0                     &m_{\chi_1}            &0          \\
         0                     &\frac{v}{\sqrt{2}}h_2 &0                     &m_{\chi_2}
         \end{pmatrix}\;.
\end{align}
Solving the eigenvalue problem gives rise to the four eigenmasses for the fermions, i.e.,
\begin{align}
    M^{(\bar{i})}_\pm &= \frac{1}{2}\Big[m_{\chi_{\bar{i}}}+m_\psi \pm
    \sqrt{(m_{\chi_{\bar{i}}}-m_\psi)^2+2v^2h_{\bar{i}}^2}\Big]\quad,\quad \bar{i}=1,2\;.
    \label{masseigen}
\end{align}



\section{Lattice notation}
For the fermion propagators we need to invert the Dirac operator:
\begin{align}
\big(\overline{\psi}\quad\overline{\chi}\big)\; D\; 
\begin{pmatrix}\psi \\ \chi\end{pmatrix} &= 
\big(\overline{\psi}\quad\overline{\chi}\big)
\begin{pmatrix}
D^{\overline{\psi}\psi} \; &D^{\overline{\psi}\chi} \\
D^{\overline{\chi}\psi} \; &D^{\overline{\chi}\chi}
\end{pmatrix}
\begin{pmatrix}\psi \\ \chi\end{pmatrix}\;,
\end{align}
with
\begin{align}
\begin{split}
D^{\overline{\psi}\psi}_{ij} &= \big(\mathrm{i}\slashed{\partial}-m_\psi\big)\delta_{ij}
-g\,\gamma^\mu A_\mu^a T^a_{ij}\;, \\
D^{\overline{\chi}\chi}_{\bar{i}\bar{j}} &= \mathrm{i}
\slashed{\partial}\delta_{\bar{i}\bar{j}} - m_{\chi_1}\delta_{\bar{i}1}\delta_{1\bar{j}}
-m_{\chi_2}\delta_{\bar{i}2}\delta_{2\bar{j}}\;, \\
D^{\overline{\psi}\chi}_{i\bar{j}} &= -h_1\tilde{\phi}_i\delta_{1\bar{j}}
-h_2\phi_i\delta_{2\bar{j}}\;, \\
D^{\overline{\chi}\psi}_{\bar{i}j} &= -h_1\tilde{\phi}_j^\star\delta_{\bar{i}1}
-h_2\phi_j^\star\delta_{\bar{i}2}\;.
\end{split}
\end{align}
Note, that this is still in Minkowski space. We now convert this step-by-step to Euclidean space on a 4-dimensional lattice. 

The first block is the standard Dirac operator for fermions as in, e.g., QCD. We use the standard Wilson-Dirac operator in this case, i.e.,
\begin{align}
    D^{\overline{\psi}\psi}(x|y)_{ij} &= \mathbbm{1}\delta_{ij} - \kappa_\psi\sum_{\mu=\pm 1}^{\pm 4}
    (\mathbbm{1}-\gamma_\mu)\,U_\mu(x)_{ij}\,\delta_{x+\hat{\mu},y}\;,
    \label{psi_dirac}
\end{align}
with the links $U_\mu(x)$, where $U_{-\mu}(x)=U_\mu(x-\hat{\mu})^\dagger$, and the definition 
$\gamma_{-\mu}=-\gamma_\mu$. Note, that $\kappa_\psi = \frac{1}{2(m_\psi+4)}$. We use Euclidean $\gamma$-matrices in the chiral representation, given by
\begin{align}
\begin{split}
    \gamma_1 &= \begin{pmatrix}
                     \phantom{-}0 &\phantom{-}0 & \phantom{-}0 &-\mathrm{i} \phantom{-} \\
                     \phantom{-}0 & \phantom{-}0 &-\mathrm{i} & \phantom{-}0 \phantom{-} \\
                     \phantom{-}0 & \phantom{-}\mathrm{i} & \phantom{-}0 & \phantom{-}0 \phantom{-} \\
                     \phantom{-}\mathrm{i} & \phantom{-}0 & \phantom{-}0 & \phantom{-}0 \phantom{-} \\
                \end{pmatrix}\;,\;
    \gamma_2 = \begin{pmatrix}
                    \phantom{-}0 & \phantom{-}0 & \phantom{-}0 &-1 \phantom{-} \\
                    \phantom{-}0 & \phantom{-}0 & \phantom{-}1 & \phantom{-}0 \phantom{-} \\
                    \phantom{-}0 & \phantom{-}1 & \phantom{-}0 & \phantom{-}0 \phantom{-} \\
                    -1 & \phantom{-}0 & \phantom{-}0 & \phantom{-}0 \phantom{-} \\
                \end{pmatrix}\;,\; \\
    \gamma_3 &= \begin{pmatrix}
                     \phantom{-}0 & \phantom{-}0 &-\mathrm{i} & \phantom{-}0 \phantom{-}\\
                    \phantom{-} 0 & \phantom{-}0 &0 &\mathrm{i} \\
                    \phantom{-} \mathrm{i} & \phantom{-}0 & \phantom{-}0 & \phantom{-}0 \phantom{-} \\
                     \phantom{-}0 &-\mathrm{i} & \phantom{-}0 & \phantom{-}0 \phantom{-} \\
                \end{pmatrix}\;,\;
    \gamma_4 = \begin{pmatrix}
                     \phantom{-}0 & \phantom{-}0 & \phantom{-}1 & \phantom{-}0 \phantom{-}\\
                     \phantom{-}0 & \phantom{-}0 & \phantom{-}0 & \phantom{-}1 \phantom{-} \\
                     \phantom{-}1 & \phantom{-}0 & \phantom{-}0 & \phantom{-}0 \phantom{-} \\
                     \phantom{-}0 & \phantom{-}1 & \phantom{-}0 & \phantom{-}0 \phantom{-} \\
                \end{pmatrix}\;,
\end{split}
\end{align}
and, as always, $\gamma_5 = \gamma_1\gamma_2\gamma_3\gamma_4$. 

The second block is the free Wilson-Dirac operator, i.e.,
\begin{align}
    D^{\overline{\chi}\chi}(x|y)_{\bar{i}\bar{j}} &= \mathbbm{1}\delta_{\bar{i}\bar{j}} - 
    \big(\kappa_{\chi_1}\delta_{\bar{i}1}\delta_{1\bar{j}}+\kappa_{\chi_2}\delta_{\bar{i}2}\delta_{2\bar{j}}\big)\sum_{\mu=\pm 1}^{\pm 4}
    (\mathbbm{1}-\gamma_\mu)\,\delta_{x+\hat{\mu},y}\;,
    \label{chi_dirac}
\end{align}
where $\kappa_{\chi_{\bar{i}}} = \frac{1}{2(m_{\chi_{\bar{i}}}+4)}$, $\bar{i}=1,2$. 

The third and fourth blocks obtain a different overall sign due to Euclidean space-time, i.e.,
\begin{align}
    D^{\overline{\psi}\chi}_{i\bar{j}} &= h_1\tilde{\phi}_i\delta_{1\bar{j}}
+h_2\phi_i\delta_{2\bar{j}}\quad,\quad
D^{\overline{\chi}\psi}_{\bar{i}j} = h_1\tilde{\phi}_j^\star\delta_{\bar{i}1}
+h_2\phi_j^\star\delta_{\bar{i}2}\;.
\label{yukawa}
\end{align}




\section{Programs}
For computing the fermion propagators, we need to invert the complete Dirac operator. By now only the standard Dirac operator \eqref{psi_dirac} is implemented in the code discussed below. We outline a strategy how to include the fermion flavor structure below.

\subsection{Inverter}

\noindent
{\bf{Notation}}

For inverting the Dirac operator, we use the BiCGStab method discussed in B. Jegerlehner, Nucl. Phys. B (Proc. Suppl.) 63, 958 (1998) (hep-lat/9612014). In order to use this method we need to know the action of the Dirac operator on a vector $v(x)_{\alpha,i}$, where $\alpha=1,2,3,4$, is a Dirac index. 

The Dirac operator \eqref{psi_dirac} with all indices reads
\begin{align}
    D^{\overline{\psi}\psi}(x|y)_{ij,\alpha\beta} &= \delta_{x,y}\delta_{\alpha\beta}\delta_{i,j} - \kappa_\psi H(x|y)_{ij,\alpha\beta}\;,
    \label{psi_dirac_index}
\end{align}
with the hopping operator
\begin{align}
    H(x|y)_{ij,\alpha\beta} &= \sum_{\mu=1}^{4}\Big[
    (\mathbbm{1}-\gamma_\mu)_{\alpha\beta}U_{\mu}(x)_{ij}\delta_{x+\hat{\mu},y}+
    (\mathbbm{1}+\gamma_\mu)_{\alpha\beta}U_{\mu}(x-\hat{\mu})^\star_{ji}\delta_{x-\hat{\mu},y}
    \Big]\;.
    \label{hopping}
\end{align}
Since the action of the first term in Equation \eqref{psi_dirac_index} is trivial, we focus on the action of the hopping matrix to the vector $v$. The resulting $4$ Dirac components of $Hv$  are:

\begin{eqnarray}
\begin{split}
    \big(H(x|y)_{ij}v(y)_j\big)_1 = \;&U_1(x)_{ij}&\big[v(x+\hat{1})_{1,j}+\mathrm{i}v(x+\hat{1})_{4,j}\big]\\
    +\;&U_1(x-\hat{1})^\star_{ji}&\big[v(x-\hat{1})_{1,j}-\mathrm{i}v(x-\hat{1})_{4,j}\big]\\
    +\;&U_2(x)_{ij}&\big[v(x+\hat{2})_{1,j}+v(x+\hat{2})_{4,j}\big]\\
     +\;&U_2(x-\hat{2})^\star_{ji}&\big[v(x-\hat{2})_{1,j}-v(x-\hat{2})_{4,j}\big]\\
     +\;&U_3(x)_{ij}&\big[v(x+\hat{3})_{1,j}+\mathrm{i}v(x+\hat{3})_{3,j}\big]\\
     +\;&U_3(x-\hat{3})^\star_{ji}&\big[v(x-\hat{3})_{1,j}-\mathrm{i}v(x-\hat{3})_{3,j}\big]\\
     +\;&U_4(x)_{ij}&\big[v(x+\hat{4})_{1,j}-v(x+\hat{4})_{3,j}\big]\\
     +\;&U_4(x-\hat{4})^\star_{ji}&\big[v(x-\hat{4})_{1,j}+v(x-\hat{4})_{3,j}\big]
\end{split}
\\
\begin{split}
    \big(H(x|y)_{ij}v(y)_j\big)_2 = \;&U_1(x)_{ij}&\big[v(x+\hat{1})_{2,j}+\mathrm{i}v(x+\hat{1})_{3,j}\big]\\
    +\;&U_1(x-\hat{1})^\star_{ji}&\big[v(x-\hat{1})_{2,j}-\mathrm{i}v(x-\hat{1})_{3,j}\big]\\
    +\;&U_2(x)_{ij}&\big[v(x+\hat{2})_{2,j}-v(x+\hat{2})_{3,j}\big]\\
     +\;&U_2(x-\hat{2})^\star_{ji}&\big[v(x-\hat{2})_{2,j}+v(x-\hat{2})_{3,j}\big]\\
     +\;&U_3(x)_{ij}&\big[v(x+\hat{3})_{2,j}-\mathrm{i}v(x+\hat{3})_{4,j}\big]\\
     +\;&U_3(x-\hat{3})^\star_{ji}&\big[v(x-\hat{3})_{2,j}+\mathrm{i}v(x-\hat{3})_{4,j}\big]\\
     +\;&U_4(x)_{ij}&\big[v(x+\hat{4})_{2,j}-v(x+\hat{4})_{4,j}\big]\\
     +\;&U_4(x-\hat{4})^\star_{ji}&\big[v(x-\hat{4})_{2,j}+v(x-\hat{4})_{4,j}\big]
\end{split}
\\ 
\begin{split}
    \big(H(x|y)_{ij}v(y)_j\big)_3 = \;&U_1(x)_{ij}&\big[v(x+\hat{1})_{3,j}-\mathrm{i}v(x+\hat{1})_{2,j}\big]\\
    +\;&U_1(x-\hat{1})^\star_{ji}&\big[v(x-\hat{1})_{3,j}+\mathrm{i}v(x-\hat{1})_{2,j}\big]\\
    +\;&U_2(x)_{ij}&\big[-v(x+\hat{2})_{2,j}+v(x+\hat{2})_{3,j}\big]\\
     +\;&U_2(x-\hat{2})^\star_{ji}&\big[v(x-\hat{2})_{2,j}+v(x-\hat{2})_{3,j}\big]\\
     +\;&U_3(x)_{ij}&\big[v(x+\hat{3})_{3,j}-\mathrm{i}v(x+\hat{3})_{1,j}\big]\\
     +\;&U_3(x-\hat{3})^\star_{ji}&\big[v(x-\hat{3})_{3,j}+\mathrm{i}v(x-\hat{3})_{1,j}\big]\\
     +\;&U_4(x)_{ij}&\big[-v(x+\hat{4})_{1,j}+v(x+\hat{4})_{3,j}\big]\\
     +\;&U_4(x-\hat{4})^\star_{ji}&\big[v(x-\hat{4})_{1,j}+v(x-\hat{4})_{3,j}\big]
\end{split}
\\ 
\begin{split}
    \big(H(x|y)_{ij}v(y)_j\big)_4 = \;&U_1(x)_{ij}&\big[v(x+\hat{1})_{4,j}-\mathrm{i}v(x+\hat{1})_{1,j}\big]\\
    +\;&U_1(x-\hat{1})^\star_{ji}&\big[v(x-\hat{1})_{4,j}+\mathrm{i}v(x-\hat{1})_{1,j}\big]\\
    +\;&U_2(x)_{ij}&\big[-v(x+\hat{2})_{1,j}+v(x+\hat{2})_{4,j}\big]\\
     +\;&U_2(x-\hat{2})^\star_{ji}&\big[-v(x-\hat{2})_{1,j}+v(x-\hat{2})_{4,j}\big]\\
     +\;&U_3(x)_{ij}&\big[v(x+\hat{3})_{4,j}+\mathrm{i}v(x+\hat{3})_{2,j}\big]\\
     +\;&U_3(x-\hat{3})^\star_{ji}&\big[v(x-\hat{3})_{4,j}-\mathrm{i}v(x-\hat{3})_{2,j}\big]\\
     +\;&U_4(x)_{ij}&\big[-v(x+\hat{4})_{2,j}+v(x+\hat{4})_{4,j}\big]\\
     +\;&U_4(x-\hat{4})^\star_{ji}&\big[v(x-\hat{4})_{2,j}+v(x-\hat{4})_{4,j}\big]
\end{split}
\end{eqnarray}

\newpage
In the program we use $a=U(v^1\pm(\mathrm{i})v^2)$ and linear combinations there of the evaluate $Hv$. Additionally we use a multi-index to collect the space-time, color, and Dirac index as
\begin{align}
   \texttt{multi\_index} &= x + V\,\alpha + 4\,V\,i\;,\;x=0,\dots,V-1\,,\;\alpha=0,1,2,3\;,\;i=0,1\;,
\end{align}
where $V$ is the volume of the lattice\footnote{Due to the counting in C++ we shift the indeces defined above by $-1$ to start with $0$.}. Note, that $x$ is also a multi-index, collecting the four space-time diractions as
\begin{align}
    x &= x_1 + N_1\,x_2 + N_1\,N_2\,x_3 + N_1\,N_2\,N_3\,x_4\;
\end{align}
where $N_\mu$, $\mu=1,2,3,4$, is the extent of the lattice in $\mu$-direction, and thus $V=N_1N_2N_3N_4$. 

\vspace*{0.5cm}
\noindent
{\bf{Adding a fermion flavor index}}

For the full propagator, one needs to add the fermion-flavor index to the multi-index as, e.g.,
\begin{align}
   \texttt{multi\_index} &= x + V\,\alpha + 4\,V\,f\;,\;f=0,1,2,3\;,
   \label{multiindex}
\end{align}
where $f=0$ corresponds to $\psi_1$, $f=1$ to $\psi_2$, $f=2$ to $\chi_1$, and $f=3$ to $\chi$. Note, that $f=0,1$ corresponds to the gauge index $i=0,1$ for $\psi$, and $f=2,3$ to the barred index 
$\bar{i}=0,1$ for $\chi_{1,2}$

Therefore, using the short-hand notation $D_{ff^\prime}$, we have
\begin{align}
\begin{split}
D_{00} &= D^{\overline{\psi}\psi}_{00}\quad,\quad D_{01} = D^{\overline{\psi}\psi}_{01}
\quad,\quad D_{02} = D^{\overline{\psi}\chi}_{00}\quad,\quad D_{03} = D^{\overline{\psi}\chi}_{01}\;,\\
D_{10} &= D^{\overline{\psi}\psi}_{10}\quad,\quad D_{11} = D^{\overline{\psi}\psi}_{11}
\quad,\quad D_{12} = D^{\overline{\psi}\chi}_{10}\quad,\quad D_{13} = D^{\overline{\psi}\chi}_{11}\;,\\
D_{20} &= D^{\overline{\chi}\psi}_{00}\quad,\quad D_{21} = D^{\overline{\chi}\psi}_{01}
\quad,\quad D_{22} = D^{\overline{\chi}\chi}_{00}\quad,\quad D_{23} = D^{\overline{\chi}\chi}_{01}\;,\\
D_{30} &= D^{\overline{\chi}\psi}_{10}\quad,\quad D_{31} = D^{\overline{\chi}\psi}_{11}
\quad,\quad D_{32} = D^{\overline{\chi}\chi}_{10}\quad,\quad D_{33} = D^{\overline{\chi}\chi}_{11}\;,
\end{split}
\label{diraccomp}
\end{align}
or to be more explicit
\begin{align}
D &= \begin{pmatrix}
&\mathbbm{1}-\kappa_\psi H_{00} & H_{01} & h_1\tilde{\phi}_0 & h_2\phi_0 \\ \\
&H_{10} &\mathbbm{1}-\kappa_\psi H_{11} & h_1\tilde{\phi}_1 & h_2\phi_1 \\ \\
&h_1\tilde{\phi}^\star_0 &h_1\tilde{\phi}^\star_1 &\mathbbm{1}-\kappa_{\chi_1} H_{00}^{\text{free}} & 0 \\ \\
&h_2\phi^\star_0 &h_2\phi^\star_1  &0 &\mathbbm{1}-\kappa_{\chi_2} H_{11}^{\text{free}}
\end{pmatrix}\;,
\label{fullD}
\end{align}
where $H^{\text{free}}$ is the same as defined in Equation \eqref{hopping} but with $U_\mu(x)=\mathbbm{1}$, $\forall x,\mu$. Note, that all elements have a (trivial) Dirac structure.

Then, the action of the full Dirac operator \eqref{psi_dirac}, \eqref{chi_dirac}, \eqref{yukawa}, on a vector $v(x)_{\alpha,i,f}$ has to be implemented.
 
\newpage
\noindent
{\bf{Code: BiCGStab.h}}

We discuss the implementation of the BiCGStab inverter for the Dirac operator \eqref{psi_dirac}. 
The inverter is called by 
\begin{align*}
    \texttt{bicgstab(out, in, kappa, diag)}\;,
\end{align*}
where $\texttt{out}$ and $\texttt{in}$ are the input and output vectors of the operation $\texttt{D}\cdot\texttt{in}=\texttt{out}$. They are $8V$-dimensional $\texttt{complex<long double>}$ arrays. The $\texttt{long double}$ parameter $\texttt{kappa}$ is the hopping parameter $\kappa_\psi$ and $\texttt{long double}$ parameter $\texttt{diag}$ is $m+4$. Thus, both possibilities 
\begin{align}
    D = (4+m) - H \quad\text{and}\quad D = 1 - \kappa H\;,
\end{align}
are implemented by setting either $\texttt{diag}=4+m$ and $\texttt{kappa}=1/2$, or 
$\texttt{diag}=1$ and $\texttt{kappa}=$ some value. 

The input vector $\texttt{in}$ corresponds to $v(x)_{\alpha,i}$ defined above. This vector can be of several shapes: point sources, plain-wave sources, etc. We should implement plain-wave sources for the invertion. 

The action of the full Dirac operator on a vector is implemented in the subroutine 
\begin{align*}
\texttt{Dirac\_on\_vec(out, in, kappa, diag)}\;.
\end{align*}

\vspace*{0.5cm}
\noindent
{\bf{Check of the inverter}}

The inverter is checked by the free case, i.e., $U_\mu(x) = \mathbbm{1}$, $\forall x$, in the program $\tt{test.cpp}$. There, point sources are implemented, i.e., 
$\delta_{x,x_0}\delta_{\alpha\alpha_0}\delta_{ii_0}$. 

The analytic propagator can be obtained by Fourier transformation, and is given by
\begin{align}
    D^{-1}(p) &=\frac{\Big[m + \sum_{\mu=1}^4\big(1-\cos p_\mu\big)\Big]\mathbbm{1}
    -\mathrm{i}\sum_{\mu=1}^4\gamma_\mu\sin p_\mu}
    {\Big[m +  \sum_{\mu=1}^4\big(1-\cos p_\mu\big)\Big]^2+\sum_{\mu=1}^4\sin^2 p_\mu}\;,
\end{align}
with $p_\mu=\frac{2\pi}{N_\mu}k_\mu$, $k_\mu=0,1,\dots,\frac{N_\mu}{2}-1$. Note, that anti-periodic boundary conditions for the fermion fields can be implemented by setting 
$p_4=\frac{2\pi}{N_4}(k_4+\frac{1}{2})$. 

We have checked the code against this result, or the more precise, against the dressing functions $A$ and $B$ defined as
\begin{align}
\begin{split}
    p_4 A_{\text{lat}}(p) &= 4\mathrm{i}\frac{\mathrm{tr}\,\gamma_4 D^{-1}(p)}
    {\big(\mathrm{tr}\,D^{-1}(p)\big)^2+\big(\mathrm{i}\,\mathrm{tr}\,\gamma_4D^{-1}(p)\big)^2}\;,\\
     B_{\text{lat}}(p) &= 4\frac{\mathrm{tr}\,D^{-1}(p)}
    {\big(\mathrm{tr}\,D^{-1}(p)\big)^2+\big(\mathrm{i}\,\mathrm{tr}\,\gamma_4D^{-1}(p)\big)^2}\;.
\end{split}
\label{dressing}
\end{align}
The analytic results for the free case are
\begin{align}
    p_4A_{\text{ana}}(p) &= \sin p_4 \quad\text{and}\quad B_{\text{ana}}(p) = m + 1 - \cos p_4\;.
\end{align}

The results for \eqref{dressing} of the inverter are compared to the analytic counter parts in Figure \ref{fig1}.
\begin{figure}[htb!]
\begin{center}
    \includegraphics[width=1\textwidth]{check_plot.pdf}
    \label{fig1}
    \caption{Results for the dressing functions $A$ and $B$ on a $4^3\times 20$ lattice. The analytic results lie on top of the lattice results.}
\end{center}
\end{figure}

A second check has been performed with non-trivial flavor structure as in Equation \eqref{fullD}, i.e., we set $h_1=h_2=0.8$, $\phi=(0,1)$, and 
$\kappa_\psi=\kappa_{\chi_1}=\kappa_{\chi_2}=0.1$ and computed $\mathrm{tr}\,D^{-1}$. We cross-checked the obtained value of $2672.23$ with the one obtained from the Mathematica notebook 
$\tt{check\_yukawa.nb}$, where we implemented the same computation in a symbolic way. Both results coincide. 


\subsection{Reading the fields}
The gauge and scalar field configurations are created in a quenched simulation by Axel. These 
configurations are stored at $\tt{143.50.77.157}$ in the folder $\tt{/data/hym/L-beta-kappa-lambda/}$, where the actual values of the parameters $\texttt{beta, kappa, lambda}$ are used to name the folder. The filea names are of the form 
\begin{align*}
&\texttt{conf-L-beta-kappa-lambda-\#config}\;, \\
&\texttt{conf-gf-L-beta-kappa-lambda-\#config}\;,
\end{align*}
where the files with label $\texttt{gf}$ are the gauge-fixed configurations. The data in these files are binary. 

The program $\tt{reader.cpp}$ reads and stores the configurations. Check sums in these files are the plaquette expectation value and the Higgs length. Both observables are computed in the reader and compared to the ones stored in the files. 

The files are structured as follows:
\begin{eqnarray*}
&\texttt{version}\qquad&\texttt{// long double} \\
&\texttt{dim}\qquad&\texttt{// int} \\
&\texttt{Nx}\qquad&\texttt{// int} \\
&\texttt{Ny}\qquad&\texttt{// int} \\
&\texttt{Nz}\qquad&\texttt{// int} \\
&\texttt{Nt}\qquad&\texttt{// int} \\
&\texttt{plaquette}\qquad&\texttt{// long double} \\
&\texttt{Higgs length}\qquad&\texttt{// long double} \\
&\texttt{A[x=0,y=0,z=0,t=0;mu=0;a=0]} \qquad&\texttt{// long double}\\
&\vdots & \vdots \\
&\texttt{A[x=0,y=0,z=0,t=0;mu=0;a=3]} \qquad&\texttt{// long double}\\
&\vdots & \vdots \\
&\texttt{A[x=0,y=0,z=0,t=0;mu=3;a=2]} \qquad&\texttt{// long double}\\
&\texttt{phi[x=0,y=0,z=0,t=0;i=0]} \qquad&\texttt{// complex<long double>}\\
&\texttt{phi[x=0,y=0,z=0,t=0;i=1]} \qquad&\texttt{// complex<long double>}\\
&\texttt{A[x=1,y=0,z=0,t=0;mu=0;a=0]} \qquad&\texttt{// long double}\\
&\vdots &\vdots\\
&\texttt{A[x=Nx-1,y=Ny-1,z=Nz-1,t=Nt-1;mu=3,a=3]} \qquad&\texttt{// long double}\\
&\texttt{phi[x=Nx-1,y=Ny-1,z=Nz-1,t=Nt-1;i=0]} \qquad&\texttt{// complex<long double>}\\
&\texttt{phi[x=Nx-1,y=Ny-1,z=Nz-1,t=Nt-1;i=1]} \qquad&\texttt{// complex<long double>}
\end{eqnarray*}
Note, that the zero-component of the gauge-field $A^0$ is stored as well.

Reading the data is achieved by calling $\texttt{read\_params(filename)}$, where $\tt{filename}$ is a character, and by calling $\texttt{read\_fields()}$. After calling the 
latter, the scalar fields are stored in the $\texttt{complex<long double>}$ array 
$\texttt{phi[x][i]}$, $\texttt{x}=0,1,\dots,V-1$, $\texttt{i}=0,1$, and the links are stored in the $\texttt{complex<long double>}$ array $\texttt{U[x][mu][i][j]}$, $\texttt{x}=0,1,\dots,V-1$, $\texttt{mu}=0,1,2,3$, $\texttt{i,j}=0,1$. The links $U$ are constructed from the gauge fields $A$ by
\begin{align}
U_\mu(x) &= \begin{pmatrix}
&A_\mu^0(x) + \mathrm{i}A_\mu^3(x) &\mathrm{i}A_\mu^1(x) + A_\mu^2(x) \\ \\
&\mathrm{i}A_\mu^1(x) - A_\mu^2(x) &A_\mu^0(x) - \mathrm{i}A_\mu^3(x)
\end{pmatrix}\;.
\end{align}

For evaluating the check sums, the subroutines $\texttt{higgs\_length()}$ and 
$\texttt{plaquette()}$ are implemented.

\section{Observables and FMS}
Interesting gauge-invariant observables are
\begin{align}
\phi^\dagger\psi &\stackrel{\text{FMS}}{\propto}\psi_2 +\cdots \quad,\quad
\tilde{\phi}^\dagger\psi \stackrel{\text{FMS}}{\propto}\psi_1 +\cdots\quad,\quad
\chi_1\quad,\quad\chi_2\;.
\end{align}
The first two operators are bound-states and expand in FMS to the gauge-variant fields $\psi_{1,2}$. The last two fields are gauge-singlets and thus are observable.

Computing the correlator by using Wick contractions gives
\begin{align*}
\big\langle\big(\phi^\dagger\psi_\alpha\big)(x)\,\overline{\big(\phi^\dagger\psi_\beta\big)}(y)\big\rangle &= \big\langle\phi^\star_i(x)\,D^{-1}(x|y)_{ij,\alpha\beta}\,\phi_j(y)\big\rangle\;,\;i,j=0,1\;,\\
\big\langle\big(\tilde{\phi}^\dagger\psi_\alpha\big)(x)\,\overline{\big(\tilde{\phi}^\dagger\psi_\beta\big)}(y)\big\rangle &= \big\langle \epsilon_{ij}\phi_j(x)\,D^{-1}(x|y)_{ik,\alpha\beta}\,\epsilon_{kl}\phi^\star_l(y)\big\rangle\;,\;i,j,k,l=0,1\;,\\
\big\langle\chi_{1,\alpha}(x)\overline{\chi}_{1,\beta}(y)\big\rangle &= \big\langle D^{-1}(x|y)_{22,\alpha\beta}\big\rangle\;, \\
\big\langle\chi_{2,\alpha}(x)\overline{\chi}_{2,\beta}(y)\big\rangle &= \big\langle D^{-1}(x|y)_{33,\alpha\beta}\big\rangle \;,
\end{align*}
where we used the conventions of Equation \eqref{multiindex} and \eqref{diraccomp} for the flavor index of the Dirac operator.

Note, that in order to compare the masses of these states to the tree-level ones \eqref{masseigen}, a rotation to the mass eigenbasis will be necessary.

\vspace*{0.2cm}
Other interesting gauge-invariant bound sates are
\begin{align}
\chi_{\bar{i}}\, \phi^\dagger\psi &\stackrel{\text{FMS}}{\propto}\chi_{\bar{i}}\,\psi_2 +\cdots \quad,\quad
\chi_{\bar{i}} \,\tilde{\phi}^\dagger\psi \stackrel{\text{FMS}}{\propto}\chi_{\bar{i}}\,\psi_1 +\cdots \quad,\quad\bar{i}=1,2\;.
\end{align}
The corresponding correlators are given by
\begin{align*}
\big\langle\big(\chi_{\bar{i}\alpha^\prime}\,\phi^\dagger\psi_\alpha\big)(x)\,\overline{\big(\chi_{\bar{i}\beta^\prime}\,\phi^\dagger\psi_\beta\big)}(y)\big\rangle &= 
\Big[
\big\langle D^{-1}(x|y)_{\bar{i}+1,\bar{i}+1,\alpha^\prime\beta^\prime}\,\phi^\star_i(x)\,
D^{-1}(x|y)_{ij,\alpha\beta}\,\phi_j(y)\big\rangle
\\&-
\phantom{\Big[}
\big\langle\phi^\star_i(x)\,D^{-1}(x|y)_{i,\bar{i}+1,\alpha\beta^\prime}\,
D^{-1}(x|y)_{\bar{i}+1,j,\alpha^\prime\beta}\,\phi_j(y)
\big\rangle
\Big]\;,\\
\big\langle\big(\chi_{\bar{i}\alpha^\prime}\,\tilde{\phi}^\dagger\psi_\alpha\big)(x)\,\overline{\big(\chi_{\bar{i}\beta^\prime}\,\tilde{\phi}^\dagger\psi_\beta\big)}(y)\big\rangle &= 
\Big[
\big\langle D^{-1}(x|y)_{\bar{i}+1,\bar{i}+1,\alpha^\prime\beta^\prime}\,\epsilon_{ij}\phi_j(x)\,
D^{-1}(x|y)_{ik,\alpha\beta}\,\epsilon_{kl}\phi^\star_l(y)\big\rangle
\\&-
\phantom{\Big[}
\big\langle\epsilon_{ij}\phi_j(x)\,D^{-1}(x|y)_{i,\bar{i}+1,\alpha\beta^\prime}\,
D^{-1}(x|y)_{\bar{i}+1,k,\alpha^\prime\beta}\,\epsilon_{kl}\phi^\star_l(y)
\big\rangle
\Big]\;,
\end{align*}
where $i,j,k,l=0,1$ (summed over) and $\bar{i}=1,2$ (not summed).

\end{document}
