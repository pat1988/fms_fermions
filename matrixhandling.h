/*---------------------------------------------------------------------------------------
	Header 		: matrixhandling.h
	Author 		: Pascal Toerek
	Date   		: 11/2015
	Description : This header defines some matrix operations.
---------------------------------------------------------------------------------------*/

#ifndef _MATRIXHANDLING_H
#define _MATRIXHANDLING_H

// set 3x3 matrix two zero
void zero(complex<long double> a[nn][nn])
{
	for(int i = 0; i < nn; i++)
	{
		for(int j = 0; j < nn; j++)
		{
			a[i][j] = complex<long double>(0.0L,0.0L);
		}
	}
}

// multiply a real number to a 2x2 matrix: C = z*A
void za(complex<long double> c[nn][nn], long double z, complex<long double> a[nn][nn])
{
	for(int i = 0; i < nn; i++)
	{
		for(int j = 0; j < nn; j++)
		{
			c[i][j] = complex<long double>(z,0)*a[i][j];
		}
	}
}

void zacomp(complex<long double> c[nn][nn], complex<long double> z, complex<long double> a[nn][nn])
{
	for(int i = 0; i < nn; i++)
	{
		for(int j = 0; j < nn; j++)
		{
			c[i][j] = z*a[i][j];
		}
	}
}

// set two matrices equal: A = B
void aeb(complex<long double> a[nn][nn], complex<long double> b[nn][nn])
{
	for(int i = 0; i < nn; i++)
	{
		for(int j = 0; j < nn; j++)
		{
			a[i][j] = b[i][j];
		}
	}
}


// add two matrices and overwrite one: C <- A + C
void apb(complex<long double> c[nn][nn], complex<long double> a[nn][nn])
{
	for(int i = 0; i < nn; i++)
	{
		for(int j = 0; j < nn; j++)
		{
			c[i][j] = a[i][j] + c[i][j];
		}
	}
}

// add two matrices: C = A + B
void capb(complex<long double> c[nn][nn], complex<long double> a[nn][nn], complex<long double> b[nn][nn])
{
	for(int i = 0; i < nn; i++)
	{
		for(int j = 0; j < nn; j++)
		{
			c[i][j] = a[i][j] + b[i][j];
		}
	}
}

// substract two matrices: C = A - B
void amb(complex<long double> c[nn][nn],complex<long double> a[nn][nn], complex<long double> b[nn][nn])
{
	for(int i = 0; i < nn; i++)
	{
		for(int j = 0; j < nn; j++)
		{
			c[i][j] = a[i][j] - b[i][j];
		}
	}
}

// multiply two matrices: C = A x B
void axb(complex<long double> c[nn][nn], complex<long double> a[nn][nn],complex<long double> b[nn][nn])
{
	for(int i = 0; i < nn; i++)
	{
		for(int j = 0; j < nn; j++)
		{
			c[i][j] = complex<long double> (0.0L,0.0L);
		   for(int k = 0; k < nn; k++)
		   {
				c[i][j] = c[i][j] + a[i][k]*b[k][j];
			}
		}
	}
}

// multiply amatrix with a hermetian conjugated 3x3 matrix: C = A x B*
void axbdag(complex<long double> c[nn][nn], complex<long double> a[nn][nn],complex<long double> b[nn][nn])
{
	for(int i = 0; i < nn; i++)
	{
		for(int j = 0; j < nn; j++)
		{
			c[i][j] = complex<long double> (0.0L,0.0L);
			for(int k = 0; k < nn; k++)
			{
				c[i][j] = c[i][j] + a[i][k]*conj(b[j][k]);
			}
		}
	}
}

// multiply a hermetian conjugated matrix with a 3x3 matrix: C = A* x B
void adagxb(complex<long double> c[nn][nn], complex<long double> a[nn][nn],complex<long double> b[nn][nn])
{
	for(int i = 0; i < nn; i++)
	{
		for(int j = 0; j < nn; j++)
		{
			c[i][j] = complex<long double> (0.0L,0.0L);
			for(int k = 0; k < nn; k++)
			{
				c[i][j]=c[i][j]+conj(a[k][i])*b[k][j];
			}
		}
	}
}


// multiply a hermetian conjugated matrix with a hermetian conjugated 3x3 matrix: C = A* x B*
void adagxbdag(complex<long double> c[nn][nn], complex<long double> a[nn][nn],complex<long double> b[nn][nn])
{
	for(int i = 0; i < nn; i++)
	{
		for(int j = 0; j < nn; j++)
		{
			c[i][j] = complex<long double> (0.0L,0.0L);
			for(int k = 0; k < nn; k++)
			{
				c[i][j]=c[i][j] + conj(a[k][i])*conj(b[j][k]);
			}
		}
	}
}

// calculate the trace of two multiplied matrices: tr[A x B]
complex<long double> multtrace(complex<long double> a[nn][nn], complex<long double> b[nn][nn])
{
	complex <long double> tr;
	tr = complex<long double> (0.0L,0.0L);

	for(int k = 0; k < nn; k++)
	{
		for(int i = 0; i < nn; i++)
		{
			tr = tr + a[i][k]*b[k][i];
		}
	}
	return tr;
}

// calculate the trace of a matrix: tr[A]
complex<long double> trace(complex<long double> a[nn][nn])
{
	complex<long double> tr;
	tr = complex<long double> (0.0L,0.0L);

	for(int i = 0; i < nn; i++)
	{
		tr = tr + a[i][i];
	}
	return tr;
}

// calculate the determinant of a 2x2 matrix: det[A]
complex<long double> det(complex<long double> a[nn][nn])
{
	complex<long double> deta;

	deta = a[0][0]*a[1][1] - a[0][1]*a[1][0];
	return deta;
}

// Multiplication of a vector with a constant
void cphi(long double c, complex<long double> phi[nn])
{
   for(int i = 0; i < nn; i++) phi[i] = c*phi[i];
}

// Matrix multiplication with vector: a = U b
void uxphi(complex<long double> phit[nn], complex<long double> U[nn][nn], complex<long double> phi[nn])
{
	for(int i = 0; i < nn; i++)
	{
	   phit[i] = 0.0L;
		for(int j = 0; j < nn; j++)
		{
			phit[i] += U[i][j]*phi[j];
		}
	}
}

void udagxphi(complex<long double> phit[nn], complex<long double> U[nn][nn], complex<long double> phi[nn])
{
	for(int i = 0; i < nn; i++)
	{
		phit[i] = 0.0L;
		for(int j = 0; j < nn; j++)
		{
			phit[i] += conj(U[j][i])*phi[j];
		}
	}
}

// Addition of two vectors: a = b + c
void phi1pphi2(complex<long double> phia[nn], complex<long double> phib[nn], complex<long double> phic[nn])
{
	for(int i = 0; i < nn; i++)
	{
		phia[i] = phib[i]+phic[i];
	}
}

// Difference of two vectors: a = b - c
void phi1mphi2(complex<long double> phid[nn], complex<long double> phia[nn], complex<long double> phib[nn])
{
	for(int i = 0; i < nn; i++)
	{
		phid[i] = phia[i]-phib[i];
	}
}

// scalar product of two complex vectors: (a,b) = a* b
complex<long double> phi1xphi2(complex<long double> phia[nn], complex<long double> phib[nn])
{
	complex<long double> dot = 0.0L;
	for(int i = 0; i < nn; i++)
	{
		dot += conj(phia[i])*phib[i];
	}
	return dot;
}

void phi1ephi2(complex<long double> phi1[nn], complex<long double> phi2[nn])
{
	for(int i = 0; i < nn; i++) phi1[i] = phi2[i];
}

#endif
