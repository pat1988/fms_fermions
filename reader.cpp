#include<iostream>
#include<cmath>
#include<complex>
#include<stdlib.h>
#include<stdio.h>
#include<fstream>
using namespace std;

ifstream input;

int nmeas, dII, nx, ny, nz, nt, nsite;
const int nn = 2;
const int nc = nn*nn - 1;
const int ncolor = nc;
const int ndirac = 4;

// Conjugate gradient parameters
const int niter_bicstab = 100;
const long double bicstab_tol = 10e-16;

// mass term for Dirac operator and Yukawa => put in input file
const long double mass = 0.50L;
const long double diag = 4.00L + 0.50L;
const long double kapp = 0.50L;
const long double yy = 1.0L;

int **neib;
long double ***A;
complex<long double> ****U;
complex<long double> **phi;

complex<long double> II = {0.0L,1.0L};

#include "matrixhandling.h"
#include "BiCGStab.h"

void extractU(int ns, int mu, complex<long double> Uout[nn][nn]);
long double plaquette();
long double higgs_length();
void allocate_arrays();
void read_params(char *file);
void read_fields();
void neib_init();

int main(int argc, char *argv[])
{
    read_params(argv[1]);
    allocate_arrays();
    neib_init();
    read_fields();

    cout << "Checks:" << endl;
    long double rho = higgs_length();
    long double plaq = plaquette();
    cout << " plaq    : " << plaq << endl;
    cout << " rho     : " << rho << endl << endl;

    input.close();
}

long double higgs_length()
{
    long double sum = 0.0;
    for(int is = 0; is < nsite; is++)
    {
        sum += sqrt(real(
               conj(phi[is][0])*phi[is][0]
            +  conj(phi[is][1])*phi[is][1]));
    }
    return sum/(1.0L*(nsite));
}

long double plaquette()
{
   int ispmu, ispnu;
   long double sumplaqs = 0.0L;
   complex<long double> U1[nn][nn], U2[nn][nn], U3[nn][nn], U4[nn][nn],
                        U23[nn][nn], U234[nn][nn];

   for(int is = 0; is < nsite; is++)
   {
       for(int mu = 0; mu < dII; mu++)
       {
           ispmu = neib[is][mu];
           for(int nu = mu + 1; nu < dII; nu++)
           {
               ispnu = neib[is][nu];

               extractU(is,mu,U1);
               extractU(ispmu,nu,U2);
               extractU(ispnu,mu,U3);
               extractU(is,nu,U4);

               axbdag(U23,U2,U3);
               axbdag(U234,U23,U4);

               sumplaqs = sumplaqs + real(multtrace(U1,U234));
           }
       }
   }

   return sumplaqs/(12.0L*nsite);
}

void extractU(int ns, int mu, complex<long double> Uout[nn][nn])
{
        for(int i = 0; i < nn; i++){
        for(int j = 0; j < nn; j++){
            Uout[i][j] = U[ns][mu][i][j];
        }}
}


void read_params(char *file)
{
    long double version;
    long double plaq, length;

    input.open(file,ios::in|ios::binary);
    input.read((char*)&version,sizeof(long double));
    input.read((char*)&dII,sizeof(int));
    input.read((char*)&nx,sizeof(int));
    input.read((char*)&ny,sizeof(int));
    input.read((char*)&nz,sizeof(int));
    input.read((char*)&nt,sizeof(int));
    input.read((char*)&plaq,sizeof(long double));
    input.read((char*)&length,sizeof(long double));

    nsite = nx*ny*nz*nt;

    cout << endl;
    cout << "Data from file: " << file << endl;
    cout << " version : " << version << endl;
    cout << " dII     : " << dII << endl;
    cout << " V       : " << nx << "x" << ny << "x" << nz << "x" << nt << endl;
    cout << " plaq    : " << plaq << endl;
    cout << " rho     : " << length << endl;
    cout << endl;
}

void read_fields()
{
    for(int t = 0; t < nt; t++){
    for(int z = 0; z < nz; z++){
    for(int y = 0; y < ny; y++){
    for(int x = 0; x < nx; x++){

        int is = x + y*nx + z*nx*ny + t*nx*ny*nz;

        for(int mu = 0; mu < dII; mu++)
        {
            long double norm = 0.0;
            for(int ic = 0; ic < nc+1; ic++)
                input.read((char*)&A[is][mu][ic],sizeof(long double));

            U[is][mu][0][0] = A[is][mu][0] + II*A[is][mu][3];
            U[is][mu][0][1] = II*A[is][mu][1] + A[is][mu][2];
            U[is][mu][1][0] = II*A[is][mu][1] - A[is][mu][2];
            U[is][mu][1][1] = A[is][mu][0] - II*A[is][mu][3];
        }

        for(int in = 0; in < nn; in++)
            input.read((char*)&phi[is][in],sizeof(complex<long double>));

        }}}}
}

void allocate_arrays()
{
    neib = new int*[nsite];
    for(int is = 0; is < nsite; is++) neib[is] = new int[2*dII];

    A = new long double**[nsite];
    U = new complex<long double>***[nsite];
    phi = new complex<long double>*[nsite];
    for(int is = 0; is < nsite; is++)
    {
        A[is] = new long double*[dII];
        U[is] = new complex<long double>**[dII];
        phi[is] = new complex<long double>[nn];
        for(int id = 0; id < dII; id++)
        {
            A[is][id] = new long double[nc+1];
            U[is][id] = new complex<long double>*[nn];
            for(int in = 0; in <nn; in++)
            {
                U[is][id][in] = new complex<long double>[nn];
            }
        }
    }
}

void neib_init()
{
    int x, xp, xm, y, yp, ym, z, zp, zm, t, tp, tm;
	int is, isp1, ism1, isp2, ism2, isp3, ism3, isp4, ism4;

	for(x = 0; x < nx; x++)
	{
  		xp = x + 1;
  		xm = x - 1;
  		if(x == nx - 1) xp = 0;
  		if(x == 0)      xm = nx - 1;

  		for(y = 0; y < ny; y++)
  		{
  			yp = y + 1;
  			ym = y - 1;
  			if(y == ny - 1) yp = 0;
  			if(y == 0)      ym = ny - 1;

  			for(z = 0; z < nz; z++)
  			{
  				zp = z + 1;
  				zm = z - 1;
  				if(z == nz - 1) zp = 0;
  				if(z == 0)      zm = nz - 1;

  				for(t = 0; t < nt; t++)
  				{
  					tp = t + 1;
  					tm = t - 1;
  					if(t == nt - 1) tp = 0;
  					if(t == 0)      tm = nt - 1;

  					is   = x  + y*nx  + z*nx*ny  + t*nx*ny*nz;
  					isp1 = xp + y*nx  + z*nx*ny  + t*nx*ny*nz;
  					ism1 = xm + y*nx  + z*nx*ny  + t*nx*ny*nz;
  					isp2 = x  + yp*nx + z*nx*ny  + t*nx*ny*nz;
  					ism2 = x  + ym*nx + z*nx*ny  + t*nx*ny*nz;
  					isp3 = x  + y*nx  + zp*nx*ny + t*nx*ny*nz;
  					ism3 = x  + y*nx  + zm*nx*ny + t*nx*ny*nz;
  					isp4 = x  + y*nx  + z*nx*ny  + tp*nx*ny*nz;
  					ism4 = x  + y*nx  + z*nx*ny  + tm*nx*ny*nz;

  					neib[is][0] = isp1;
  					neib[is][1] = isp2;
  					neib[is][2] = isp3;
  					neib[is][3] = isp4;
  					neib[is][4] = ism1;
  					neib[is][5] = ism2;
  					neib[is][6] = ism3;
  					neib[is][7] = ism4;
  				}
  			}
  		}
	}
}
