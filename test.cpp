#include<iostream>
#include<cmath>
#include<complex>
#include<stdlib.h>
#include<stdio.h>
using namespace std;

const int lengX = 4;
const int lengY = 4;
const int lengZ = 4;
const int lengT = 20;
const int nsite = lengX*lengY*lengZ*lengT; // lattice size
const int ndirac = 4; // Dirac indices
const int ncolor = 2; // number of colors
const int nall = nsite*ndirac*ncolor; // full dimension

long double m = 0.5L; // mass
long double diag = m + 4.0L; // diagonal part of Dirac operator
long double kappa = 0.5L;    // hopping parameter

const int niter_bicstab = 100; // max. number of bicstab iterations
long double bicstab_tol = 10e-4; // bicstab tolcerance

complex<long double> I = {0.0,1.0};

int **neib;
complex<long double> ****gaugefield;
complex<long double> **prop;

#include "BiCGStab.h"

int main()
{
	// define gaugefield
	gaugefield = new complex<long double>***[nsite];
	for(int is = 0; is < nsite; is++){
		gaugefield[is] = new complex<long double>**[ndirac];
		for(int mu = 0; mu < ndirac; mu++){
			gaugefield[is][mu] = new complex<long double>*[ncolor];
			for(int a = 0; a < ncolor; a++)
			{
				gaugefield[is][mu][a] = new complex<long double>[ncolor];
			}
		}
	}
	// define space-time neighbors
	neib = new int*[nsite];
	for(int is = 0; is < nsite; is++) neib[is] = new int[8];

	// propagator
	prop = new complex<long double>*[nall];
	for(int id1 = 0; id1 < nall; id1++) prop[id1] = new complex<long double>[nall];

	// check the free case
    for(int is = 0; is <  nsite; is++){
    for(int id = 0; id < ndirac; id++){
    for(int a  = 0; a  < ncolor;  a++){
    for(int b  = 0; b  < ncolor;  b++){
        gaugefield[is][id][a][b] = 0.0;
        if(a == b) gaugefield[is][id][a][b] = 1.0;
    }}}}

	// initialize neighbor array (in BiCGStab.h)
    neib_init(lengX,lengY,lengZ,lengT);

    complex<long double> *in;
	in = new complex<long double>[nall];
    complex<long double> *out;
	out = new complex<long double>[nall];

	// invert on point-sources
    for(int id1 = 0; id1 < nall; id1++)
    {
        for(int id2 = 0; id2 < nall; id2++)
        {
			// point source
            in[id2] = 0.0L;
            if(id1==id2) in[id2] = 1.0L;
			// initial value
            out[id2] = 0.0L;
        }
		// invert
        bicgstab(in,out);
		// build propagator
		for(int id2 = 0; id2 < nall; id2++) prop[id1][id2] = out[id2];
    }

	// compute dressing functions
	long double pi2 = 8.0L*atan(1.0L);
	complex<long double> phase;
	complex<long double> d11[lengT], d13[lengT], d22[lengT], d24[lengT],
	                     d31[lengT], d33[lengT], d42[lengT], d44[lengT];
	for(int ip = 0; ip <= lengT/2; ip++){
		long double mom = ip*1.0L;
		d11[ip] = 0.0L;
		d13[ip] = 0.0L;
		d22[ip] = 0.0L;
		d24[ip] = 0.0L;
		d31[ip] = 0.0L;
		d33[ip] = 0.0L;
		d42[ip] = 0.0L;
		d44[ip] = 0.0L;
		for(int x1 = 0; x1 < lengX; x1++){
		for(int x2 = 0; x2 < lengY; x2++){
		for(int x3 = 0; x3 < lengZ; x3++){
		for(int x4 = 0; x4 < lengT; x4++){
			int x = x1 + lengX*x2 + lengX*lengY*x3 + lengX*lengY*lengZ*x4;
			int i11 = x;
			int i12 = x + 4*nsite;
			int i21 = x + nsite;
			int i22 = x + 5*nsite;
			int i31 = x + 2*nsite;
			int i32 = x + 6*nsite;
			int i41 = x + 3*nsite;
			int i42 = x + 7*nsite;
			for(int y1 = 0; y1 < lengX; y1++){
			for(int y2 = 0; y2 < lengY; y2++){
			for(int y3 = 0; y3 < lengZ; y3++){
			for(int y4 = 0; y4 < lengT; y4++){
				phase = polar(1.0L,pi2/(lengT)*mom*(y4-x4));
				int y = y1 + lengX*y2 + lengX*lengY*y3 + lengX*lengY*lengZ*y4;
				int j11 = y;
				int j12 = y + 4*nsite;
				int j21 = y + nsite;
				int j22 = y + 5*nsite;
				int j31 = y + 2*nsite;
				int j32 = y + 6*nsite;
				int j41 = y + 3*nsite;
				int j42 = y + 7*nsite;

				d11[ip] += phase*(prop[i11][j11] + prop[i12][j12]);
				d13[ip] += phase*(prop[i11][j31] + prop[i12][j32]);
				d22[ip] += phase*(prop[i21][j21] + prop[i22][j22]);
				d24[ip] += phase*(prop[i21][j41] + prop[i22][j42]);
				d31[ip] += phase*(prop[i31][j11] + prop[i32][j12]);
				d33[ip] += phase*(prop[i31][j31] + prop[i32][j32]);
				d42[ip] += phase*(prop[i41][j21] + prop[i42][j22]);
				d44[ip] += phase*(prop[i41][j41] + prop[i42][j42]);
			}}}}
		}}}}
		d11[ip] = d11[ip]/(2.0L*nsite);
		d13[ip] = d13[ip]/(2.0L*nsite);
		d22[ip] = d22[ip]/(2.0L*nsite);
		d24[ip] = d24[ip]/(2.0L*nsite);
		d31[ip] = d31[ip]/(2.0L*nsite);
		d33[ip] = d33[ip]/(2.0L*nsite);
		d42[ip] = d42[ip]/(2.0L*nsite);
		d44[ip] = d44[ip]/(2.0L*nsite);
	}

	complex<long double> tr1d, tr4d;
	complex<long double> pA[lengT], B[lengT];
	for(int ip = 0; ip < lengT; ip++)
	{
		tr1d = d11[ip] + d22[ip] + d33[ip] + d44[ip];
		tr4d = d31[ip] + d42[ip] + d13[ip] + d24[ip];

		complex<long double> sq = tr1d*tr1d - tr4d*tr4d;

		pA[ip] = tr4d/sq;
		 B[ip] = tr1d/sq;
	}

	cout << endl;
	cout << "----- Dressing functions -----" << endl;
	for(int ip = 0; ip <= lengT/2; ip++)
	{
		long double mom = ip*1.0L;
		cout << ip << " " << 2.0L*abs(sin(pi2/(2.0L*lengT)*mom)) << " "
		     << imag(pA[ip]) << " "
			 << real(B[ip])  << endl;
	}

    return 0;
}
